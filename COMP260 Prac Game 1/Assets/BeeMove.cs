﻿
using UnityEngine;
using System.Collections;

public class BeeMove : MonoBehaviour {
	//public float speed = 4.0f;
	//public float turnSpeed = 180.0f;
	public Transform target; 
	public Transform target2;
	public Vector2 heading = Vector3.right;

	public float minSpeed, maxSpeed;
	public float minTurnSpeed, maxTurnSpeed;

	private float speed; 
	private float turnSpeed;

	public ParticleSystem explosionPrefab;

	// Use this for initialization
	void Start () {
		Player1Move player = (Player1Move)FindObjectOfType (typeof(Player1Move));
		target = player.transform;

		Player2Move player2 = (Player2Move)FindObjectOfType (typeof(Player2Move));
		target2 = player2.transform;

		heading = Vector2.right;
		float angle = Random.value * 360;
		heading = heading.Rotate (angle);

		speed = Mathf.Lerp (minSpeed, maxSpeed, Random.value);
		turnSpeed = Mathf.Lerp (minTurnSpeed, maxTurnSpeed, Random.value);

	}

	void OnDestroy(){
		ParticleSystem explosion = Instantiate (explosionPrefab);
		explosion.transform.position = transform.position;

		Destroy (explosion.gameObject, explosion.duration);


	}

	// Update is called once per frame
	void Update () {


		Vector2 direction = target.position - transform.position;
		Vector2 direction2 = target2.position - transform.position;


		//Vector2 Close;
		if (direction.magnitude > direction2.magnitude) {
			//Close = direction;
			float angle = turnSpeed * Time.deltaTime;
			//} else {
			//	Close = direction2; 
			//}
			//float angle = turnSpeed * Time.deltaTime;


			if (direction2.IsOnLeft (heading)) {
				heading = heading.Rotate (angle);
			} else {
				heading = heading.Rotate (-angle);
			}
			Vector2 velocity = direction * speed;


			transform.Translate (heading * speed * Time.deltaTime);
			transform.Translate(velocity * Time.deltaTime);
		} else {
			float angle = turnSpeed * Time.deltaTime;
			if (direction.IsOnLeft (heading)) {
				heading = heading.Rotate (angle);
			} else {
				heading = heading.Rotate (-angle);
			}
			transform.Translate (heading * speed * Time.deltaTime);
		}
	}
	//void OnDrawGizmos(){
	//	Gizmos.color = Color.red;
	//	Gizmos.DrawRay (transform.position, heading);

	//	Gizmos.color = Color.yellow;
	//	Vector2 direction = target.position - transform.position;
	//	Vector2 direction2 = target2.position - transform.position;

	//	Gizmos.DrawRay (transform.position, direction);
	//	Gizmos.DrawRay (transform.position, direction2);

	//}
}